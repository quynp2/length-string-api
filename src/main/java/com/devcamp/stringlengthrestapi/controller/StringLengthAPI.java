package com.devcamp.stringlengthrestapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class StringLengthAPI {
    /**
     * @param requesString
     * @return
     */
    @GetMapping("/length")
    public int StringLengthApi(@RequestParam(required = true, name = "string") String requesString) {
        int stringLength = requesString.length();
        return stringLength;

    }

}
